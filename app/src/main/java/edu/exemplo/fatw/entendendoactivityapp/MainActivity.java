package edu.exemplo.fatw.entendendoactivityapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private ArrayAdapter<String> adapter;
    private ArrayList<String> array;
    private ListView log;
    private Button limpar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        limpar = (Button) findViewById(R.id.limpar);
        log = (ListView) findViewById(R.id.log);

        array = LogDelegate.obterLog(getFileStreamPath("log"));

        boolean voltando = getIntent().getBooleanExtra("voltando",false);


        if(voltando){
            atualizarLog("Voltando da Segunda Activate");
        }else{
            atualizarLog("Iniciando log...");
        }

        log.setAdapter(adapter);
        atualizarLog("MainActivity sendo iniciada... onCreate() executado.");
    }

    @Override
    protected void onStart(){
        super.onStart();
        atualizarLog("MainActivity sendo exibida... onStart() executado.");
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        atualizarLog("MainActivity recebeu foco... onResume() executado.");
    }

    @Override
    protected void onRestart()
    {
        super.onRestart();
        atualizarLog("MainActivity voltando a ser exibida... onRestart() executado.");
    }

    @Override
    protected void onPause()
    {
        super.onPause();
        atualizarLog("MainActivity não está ativa... onPause() executado.");
    }

    @Override
    protected void onStop()
    {
        super.onStop();
        atualizarLog("MainActivity foi parada e não está visível... onStop() executado.");
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        atualizarLog("MainActivity foi encerrada... onDestroy() executado.");
    }

    public void limparLog(View view){
        atualizarLog("");
    }

    private void atualizarLog(String item){
        if(array == null){
            array = new ArrayList<String>();
        }

        if("".equalsIgnoreCase(item)){
            array.clear();
            LogDelegate.enviarLog(getFileStreamPath("log"),array);
            exibeMensagem("Limpando Log.");
        } else {
            array.add((array.size() + 1)+"º: " + item);
            exibeMensagem(item);
        }
        if(adapter != null){
            adapter.notifyDataSetChanged();
        }else{
            adapter = new ArrayAdapter<String>(getApplicationContext(),R.layout.layout_log,array);
        }

        LogDelegate.enviarLog(getFileStreamPath("log"),array);
    }

    private void exibeMensagem(String text){
        Toast.makeText(getApplicationContext(), text, Toast.LENGTH_LONG).show();
    }

    protected void irSegundaActivity(View view){
        atualizarLog("Indo para Segunda Activity");
        startActivity(new Intent(getBaseContext(), SegundaActivity.class));
    }



}
